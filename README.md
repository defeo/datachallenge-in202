Jeux de données pour le TD
<http://defeo.lu/in202/tutorials/tutorial9>.

Ce répertoire contient quatre jeux de données :

- `divvy-stations.csv` : Liste des stations du service d'emprunt de
  vélos de la ville de Chicago [Divvy](https://www.divvybikes.com/),
  au deuxième semestre 2014. (300 stations)

- `divvy-distances.csv`: Distances entre les stations du jeu
  précédent. (90000 paires de stations)

- `divvy-trips.h5`, `divvy-trips.csv` : Liste des trajets du service
  d'emprunt de vélos de la ville de Chicago
  [Divvy](https://www.divvybikes.com/), au deuxième
  semestre 2014. (1548935 trajets)

- `meteo.csv` : Données météo journalières de la ville de Chicago au
  deuxième semestre 2014. (184 jours)

Les jeux de données sur le système Divvy sont extraits du data
challenge <https://www.divvybikes.com/datachallenge>. Les données
complètes sont disponibles ici :
<https://github.com/stevevance/divvy-munging>.

Les données météo sont obtenues du service de
[données historiques de Weather Underground](http://www.wunderground.com/history/).


## Description des jeux

### Stations

Le fichier `divvy-stations.csv` contient les champs suivants, une
ligne par station :

- `id` : identifiant de la station (entier),
- `name` : nom de la station (chaîne de caractères),
- `latitude`, `longitude` : coordonnées GPS de la station (flottants),
- `dcapacity` : nombre d'emplacement vélos (entier),
- `dateCreated` : date de mise en service de la station (date M/J/AAAA).


### Distances

- `from_station_id` : identifiant de la station de départ (entier),
- `to_station_id` : identifiant de la station d'arrivée (entier),
- `distanceKM` : distance en Km entre les stations (flottant).


### Trajets

Les fichiers `divvy-trips.h5` et `divvy-trips.csv` continnent
exactement les mêmes données. Le premier fichier, au format
[HDF5](http://www.hdfgroup.org/HDF5/), occupe moins de place et peut
être importé plus facilement et rapidement avec pandas.

Les fichiers contiennent les champs suivants, une ligne par trajet :

- `trip_id` : identifiant du trajet (entier),
- `starttime` : heure de début du trajet (date),
- `stoptime` : heure de fin du trajet (date),
- `bikeid` : identifiant du vélo (entier),
- `tripduration` : durée du trajet en secondes (entier),
- `from_station_id` : identifiant de la station de départ (entier,
  voir fichier stations),
- `from_station_name` : nom de la station de départ (chaîne, voir
  fichier stations),
- `to_station_id` : identifiant de la station d'arrivée (entier, voir
  fichier stations),
- `to_station_name` : nom de la station d'arrivée (chaîne, voir
  fichier stations),
- `usertype` : type d'utilisateur (`Customer`/`Subscriber`) :
  - `Customer` : abonné une journée,
  - `Subscriber` : abonnée à l'année,
- `gender` : sexe de l'abonnée (`M/F`, uniquement pour les abonnés à
  l'année),
- `birthyear` : année de naissance de l'abonné (entier, uniquement
  pour les abonnés à l'année).


### Météo

Le fichier `meteo.csv` contient les champs suivants, une ligne par jour :

- `CST` : date de l'enregistrement (date, format ISO AAAA-MM-JJ),
- `Max TemperatureC` : temperature maximale en Celsius (entier),
- `Mean TemperatureC` : temperature moyenne en Celsius (entier),
- `Min TemperatureC` : temperature minimale en Celsius (entier),
- `Dew PointC` : Point de rosée maximal en Celsius (entier),
- `MeanDew PointC` : Point de rosée moyen en Celsius (entier),
- `MinDew PointC` : Point de rosée minimal en Celsius (entier),
- `Max Humidity` : taux d'humidité maximal en pourcentage (entier),
- `Mean Humidity` : taux d'humidité moyen en pourcentage (entier),
- `Min Humidity` : taux d'humidité minimal en pourcentage (entier),
- `Max Sea Level PressurehPa` : pression atmosphérique maximale en
  hectoPascals (entier),
- `Mean Sea Level PressurehPa` : pression atmosphérique moyenne en
  hectoPascals (entier),
- `Min Sea Level PressurehPa` : pression atmosphérique minimale en
  hectoPascals (entier),
- `Max VisibilityKm` : visibilité maximale en kilomètres (entier),
- `Mean VisibilityKm` : visibilité moyenne en kilomètres (entier),
- `Min VisibilityKm` : visibilité minimale en kilomètres (entier),
- `Max Wind SpeedKm/h` : vitesse maximale du vent en km/h (entier),
- `Mean Wind SpeedKm/h` : vitesse moyenne du vent en km/h (entier),
- `Min Wind SpeedKm/h` : vitesse minimale du vent en km/h (entier),
- `Max Gust SpeedKm/h` : vitesse maximale des rafales de vent en km/h
  (entier),
- `Precipitationmm` : millimètres de pluie (flottant),
- `CloudCover` : taux de couverture nuageuse (entier),
- `Events` : événements météo (pluie, neige, ...) (chaîne de caractères),
- `WindDirDegrees` : direction du vent en degrés (entier, entre 0° et
  359°). Le nord est à 0°, l'est à 90°.
